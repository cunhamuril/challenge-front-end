export default {
  primary: '#FCDB00',
  secondary: '#010101',

  white: '#FFFFFF',
  black: '#000000',
  lightBlack: '#333333',

  gray: '#E5E5E5',
  lightGray: '#F1F1F1',
  darkGray: '#555555',

  red: '#ff3737',
  green: '#3fa83f',
};
