import { createGlobalStyle } from 'styled-components';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  body, #root {
    width: 100%;    
    min-height: 100vh;

    font-family: 'OpenSans-Regular', sans-serif;
  }

  body {
    -webkit-font-smoothing: antialiased;
  }
`;
