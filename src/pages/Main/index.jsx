import React, { useState, useEffect } from 'react';
import Skeleton from 'react-loading-skeleton';
import { format, parseISO } from 'date-fns';

import { ReactComponent as AppleIcon } from '../../assets/icons/apple.svg';
import { ReactComponent as AndroidIcon } from '../../assets/icons/android.svg';
import { ReactComponent as WindowsIcon } from '../../assets/icons/windows.svg';
import { ReactComponent as PixterLogo } from '../../assets/images/logo.svg';

import { api } from '../../services';

import { Modal } from '../../components';

import { Carousel, Content, Info, Tablet, Books, ModalContent } from './styles';

const Main = () => {
  const [books, setBooks] = useState([]);
  const [currentBook, setCurrentBook] = useState({});
  const [modalVisibility, setModalVisibility] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchBooks = async () => {
      setLoading(true);

      const valuesToSearch = [
        'HARRY POTTER',
        'A SONG OF ICE AND FIRE',
        'LORD OF THE RINGS',
        'THE HITCHHINKERS GUIDE TO THE GALAXY',
        'THE CHRONICLES OF NARNIA',
        'THE MISTS OF AVALON',
        'ASSASSINS CREED',
        'PERCY JACKSON AND THE OLYMPIANS',
        'HP LOVECRAFT',
        'DAN BROWN',
        'GEORGE ORWELL',
        'PAULO COELHO',
        'FERNANDO PESSOA',
        'AGATHA CHRISTIE',
        'STEPHEN KING',
      ];

      const min = Math.ceil(0);
      const max = Math.floor(valuesToSearch.length);

      const randomValue =
        valuesToSearch[Math.floor(Math.random() * (max - min)) + min];

      try {
        const { data } = await api.get('/volumes', {
          params: {
            q: randomValue,
          },
        });

        if (data.items) {
          setBooks(
            data.items.filter((book) => book.volumeInfo?.imageLinks).slice(0, 8)
          );
        }
      } catch (error) {
        console.error(error.response);
      } finally {
        setLoading(false);
      }
    };

    fetchBooks();
  }, []);

  const toggleModal = () => setModalVisibility(!modalVisibility);

  const handleBookPreview = (book) => {
    const { volumeInfo } = book;

    let authors = '';
    let publishedDate = '';

    if (volumeInfo.authors?.length === 1) {
      authors = volumeInfo.authors;
    } else if (volumeInfo.authors?.length === 2) {
      authors = `${volumeInfo.authors[0]} and ${volumeInfo.authors[1]}`;
    } else if (volumeInfo.authors?.length > 2) {
      authors = volumeInfo.authors.join(', ').toString();
    }

    if (volumeInfo.publishedDate?.match(/\d{4}-\d{2}-\d{2}/)) {
      publishedDate = format(
        parseISO(volumeInfo.publishedDate),
        "MMMM dd'th', yyyy"
      );
    } else {
      publishedDate = volumeInfo.publishedDate;
    }

    const formattedBookData = {
      ...volumeInfo,
      authors,
      publishedDate,
    };

    setCurrentBook(formattedBookData);
    toggleModal();
  };

  return (
    <>
      <Carousel>
        {[1, 2, 3].map((item) => (
          <Content key={item}>
            <Info>
              <h1>Pixter Digital Books</h1>

              <h2>Lorem ipsum dolor sit amet? blablabla elit, volutpat.</h2>

              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Praesent vitae eros eget tellus tristique bibendum. Donec rutrum
                sed sem quis venenatis. Proin viverra risus a eros volutpat
                tempor. In quis arcu et eros porta lobortis sit
              </p>

              <div className="icons-wrapper">
                <AppleIcon />
                <AndroidIcon />
                <WindowsIcon />
              </div>
            </Info>

            <Tablet>
              <div className="screen">
                <PixterLogo />
              </div>
            </Tablet>
          </Content>
        ))}
      </Carousel>

      <Books>
        <h1>Books</h1>

        <h5>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
          vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
          venenatis.
        </h5>

        <div className="books-wrapper">
          {loading
            ? Array.from({ length: 8 }).map((item, index) => (
                <Skeleton key={index} width={128} height={192} />
              ))
            : books.map((book) => (
                <img
                  key={book.id}
                  src={book.volumeInfo.imageLinks.thumbnail}
                  onClick={() => handleBookPreview(book)}
                  alt="Book Thumbnail"
                />
              ))}
        </div>
      </Books>

      <Modal visible={modalVisibility} toggle={toggleModal}>
        {Object.keys(currentBook).length > 0 && (
          <ModalContent>
            <div className="info-wrapper">
              <img src={currentBook.imageLinks.thumbnail} alt="Thumbnail" />

              <div className="info">
                <h1>{currentBook.title}</h1>
                {currentBook.subtitle && <h2>{currentBook.subtitle}</h2>}

                <div className="author-publish">
                  <h4>Write by {currentBook.authors}</h4>
                  <h4>Published in: {currentBook.publishedDate}</h4>
                </div>
              </div>
            </div>

            <p className="description">{currentBook.description}</p>
          </ModalContent>
        )}
      </Modal>
    </>
  );
};

export default Main;
