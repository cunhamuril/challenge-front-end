import styled from 'styled-components';
import media from 'styled-media-query';
import { Carousel as OriginalCarousel } from 'react-responsive-carousel';

import { colors } from '../../theme';

export const Carousel = styled(OriginalCarousel).attrs({
  showArrows: false,
  showStatus: false,
  showThumbs: false,
  infiniteLoop: true,
  autoPlay: true,
  stopOnHover: true,
  interval: 5000,
})`
  display: flex;
  align-items: center;
  justify-content: center;

  height: 100vh;

  background-color: ${colors.primary};

  .slide {
    background: transparent;

    height: inherit;
  }

  .control-dots {
    bottom: 8%;

    .dot {
      width: 20px;
      height: 20px;

      box-shadow: unset;
    }
  }

  ${media.lessThan('large')`
    .control-dots {
      bottom: 4%;
    }
  `}
`;

export const Content = styled.section.attrs({ id: 'home' })`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
  height: 100vh;
  margin: 34px auto 0;
  max-width: 1200px;

  ${media.lessThan('1280px')`
    padding: 0 64px;
  `}

  ${media.lessThan('1024px')`  
    justify-content: space-evenly;
    flex-direction: column-reverse;    
  `}

  ${media.lessThan('small')`
    justify-content: center;
    padding: 0 16px;    
  `}

`;

export const Info = styled.div`
  width: 100%;
  max-width: 684px;

  text-align: start;

  h1 {
    font-size: 33px;
    line-height: 45px;
    font-family: 'OpenSans-Bold', sans-serif;
  }

  h2,
  p {
    margin-top: 36px;
  }

  h2 {
    font-size: 17px;
    line-height: 23px;
  }
  position: relative;
  p {
    font-size: 17px;
    line-height: 23px;

    font-family: 'OpenSans-Light', serif;
  }

  .icons-wrapper {
    margin-top: 38px;

    svg + svg {
      margin-left: 38px;
    }
  }

  ${media.lessThan('1024px')`
    margin: 0 auto;

    text-align: center;
  `}
`;

export const Tablet = styled.div`
  margin-top: 70px;
  margin-right: 16px;
  padding: 44px 14px;

  width: 280px;
  height: 416px;
  border-radius: 24px;

  background-color: ${colors.white};
  box-shadow: -15px -2px 35px rgba(0, 0, 0, 0.15);

  .screen {
    ${media.lessThan('1280px')`
    padding: 0 64px;
  `}
    display: flex;
    align-items: center;
    justify-content: center;

    width: 100%;
    height: 100%;

    background-color: ${colors.black};

    svg {
      filter: invert(1);
    }
  }

  ${media.lessThan('medium')`
    margin: 0;
  `}

  ${media.lessThan('small')`
    display: none;
  `}
`;

export const Books = styled.section.attrs({
  id: 'books',
})`
  display: flex;  
  flex-direction: column;
  justify-content: center;

  margin: 0 auto;
  padding: 80px 0;

  width: 100%;
  max-width: 827px;
  min-height: 60vh;

  text-align: center;

  h1 {
    font-size: 25px;
    line-height: 34px;
    font-family: 'OpenSans-Bold', sans-serif;

    color: ${colors.secondary};
  }

  h5 {
    margin-top: 28px;

    font-size: 13px;
    line-height: 18px;
    font-family: 'OpenSans-Light', sans-serif;

    color: ${colors.darkGray};
  }

  .books-wrapper {
    display: grid;
    grid-gap: 30px 105px;
    justify-items: center;
    grid-template-rows: repeat(2, 1fr);
    grid-template-columns: repeat(4, 1fr);

    margin-top: 35px;

    img {
      width: 128px;

      cursor: pointer;
      transition: all 0.3s ease;

      :hover {
        transform: scale(1.1);        
      }
    }
  }

  ${media.lessThan('medium')`
    padding: 80px 64px;

    .books-wrapper {
      grid-template-rows: repeat(3, 1fr);
      grid-template-columns: repeat(3, 1fr);
    }
  `}

  ${media.lessThan('small')`    
    padding: 80px 16px;

    .books-wrapper {
      grid-gap: 30px 0;
      grid-template-rows: repeat(4, 1fr);
      grid-template-columns: repeat(2, 1fr);
    }
  `}

  ${media.lessThan('280px')`    
    .books-wrapper {      
      grid-template-rows: repeat(8, 1fr);
      grid-template-columns: 1fr;
    }
  `}
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  width: 100%;
  height: 100%;
  padding: 40px 32px 0;

  .info-wrapper {
    display: flex;

    img {
      width: 25%;
      height: 100%;
    }

    .info {
      flex: 1;

      margin-left: 16px;

      h1 {
        font-size: 25px;

        font-family: 'OpenSans-Bold', sans-serif;
        color: ${colors.secondary};
      }

      h2 {
        font-size: 17px;

        font-family: 'OpenSans-Regular', sans-serif;
        color: ${colors.lightBlack};
      }

      .author-publish h4 {
        margin-top: 16px;

        font-size: 14px;

        font-family: 'OpenSans-Light', sans-serif;
        color: ${colors.darkGray};
      }
    }
  }

  .description {
    margin-top: 16px;

    overflow: auto;
    height: 175px;

    font-size: 14px;
    color: ${colors.secondary};
  }

  ${media.lessThan('small')`
    padding: 40px 16px 0;

    .info-wrapper {
      img {
        width: 30%;
      }

      .info {
        h1 {
          font-size: 20px;
        }

        .author-publish h4 {
          margin-top: 4px;

          font-size: 12px;
        }
      }
    }

    .description {      
      height: 210px;
    }
  `}

  ${media.lessThan('280px')`
    .description {      
      height: 190px;
    }
  `}
`;
