export default function preventBodyScroll(condition) {
  if (condition) {
    document.body.style.overflow = 'hidden';
  } else {
    document.body.style.overflow = 'auto';
  }
}
