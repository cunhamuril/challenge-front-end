import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { FiMenu, FiX } from 'react-icons/fi';

import { preventBodyScroll } from '../../functions';

import { ScrollToTopButton } from '../.';

import { Container, Navbar, MenuItems, SideMenu, MenuButton } from './styles';

import { ReactComponent as PixterLogo } from '../../assets/images/logo.svg';

const Menu = ({ side, onClick }) => (
  <MenuItems side={side}>
    <li onClick={onClick}>
      <AnchorLink href="#books" offset="60">
        Books
      </AnchorLink>
    </li>
    <li onClick={onClick}>
      <AnchorLink href="#newsletter" offset="100">
        Newsletter
      </AnchorLink>
    </li>
    <li onClick={onClick}>
      <AnchorLink href="#address">Address</AnchorLink>
    </li>
  </MenuItems>
);

const Header = () => {
  const [scrolled, setScrolled] = useState(false);
  const [isSideMenuOpen, setIsSideMenuOpen] = useState(false);

  const toggleSideMenu = () => setIsSideMenuOpen(!isSideMenuOpen);

  useEffect(() => {
    function listenScroll() {
      window.onscroll = () => {
        if (window.scrollY >= 50) {
          setScrolled(true);
        } else {
          setScrolled(false);
        }
      };
    }

    listenScroll();
  }, []);

  useEffect(() => {
    preventBodyScroll(isSideMenuOpen);
  }, [isSideMenuOpen]);

  return (
    <>
      <Container scrolled={scrolled}>
        <Navbar scrolled={scrolled}>
          <AnchorLink href="#home" offset="100">
            <PixterLogo />
          </AnchorLink>

          <Menu />

          <MenuButton onClick={toggleSideMenu}>
            <FiMenu />
          </MenuButton>
        </Navbar>
      </Container>

      <SideMenu slide right isOpen={isSideMenuOpen}>
        <div className="overlay" onClick={toggleSideMenu} />

        <div className="menu-wrapper">
          <MenuButton onClick={toggleSideMenu}>
            <FiX />
          </MenuButton>

          <Menu side onClick={toggleSideMenu} />
        </div>
      </SideMenu>

      {scrolled && <ScrollToTopButton />}
    </>
  );
};

Menu.propTypes = {
  side: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Header;
