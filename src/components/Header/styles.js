import styled, { css } from 'styled-components';
import media from 'styled-media-query';

import { colors } from '../../theme';

export const Container = styled.header`
  position: fixed;
  z-index: 1;
  right: 0;
  left: 0;
  top: 0;

  width: 100%;
  padding: ${({ scrolled }) => (scrolled ? '6px 0' : '70px 0 20px')};

  transition: padding 0.5s ease;
  background-color: ${colors.primary};

  ${({ scrolled }) =>
    scrolled &&
    css`
      box-shadow: 0px 0 14px rgba(0, 0, 0, 0.15);
    `};

  ${media.lessThan('medium')`
    padding: ${({ scrolled }) => !scrolled && '40px 0 20px'};
  `}

  ${media.lessThan('small')`
    padding: ${({ scrolled }) => !scrolled && '20px 0'};
  `}
`;

export const Navbar = styled.nav`
  display: flex;
  align-items: center;
  justify-content: space-between;

  width: 100%;
  margin: 0 auto;
  max-width: 1200px;

  a svg {
    width: ${({ scrolled }) => (scrolled ? '120px' : '149px')};

    transition: width 0.5s ease;
  }

  ${media.lessThan('1280px')`
    padding: 0 64px;
  `}

  ${media.lessThan('small')`      
    padding: 0 16px;

    ul {
      display: none;
    }
  `}
`;

export const MenuItems = styled.ul`
  display: flex;

  flex-direction: ${({ side }) => (side ? 'column' : 'row')};

  list-style: none;

  li {
    & + li {
      margin-left: ${({ side }) => (side ? 0 : '49px')};
      margin-top: ${({ side }) => (side ? '16px' : 0)};
    }

    a {
      text-decoration: none;
      border-bottom: solid 3px transparent;

      color: ${colors.secondary};
      transition: border 0.3s ease;
      font-family: 'OpenSans-Bold', sans-serif;

      &:hover,
      &.active {
        border-bottom: solid 3px ${colors.secondary};
      }
    }
  }
`;

export const MenuButton = styled.button`
  border: none;
  background-color: transparent;

  svg {
    font-size: 30px;
  }

  ${media.greaterThan('451px')`
    display: none;
  `}
`;

export const SideMenu = styled.menu`
  .overlay,
  .menu-wrapper {
    position: fixed;

    height: 100%;
  }

  .overlay {
    z-index: 1;
    display: ${({ isOpen }) => (isOpen ? 'unset' : 'none')};

    width: 100%;

    background-color: rgba(0, 0, 0, 0.5);
  }

  .menu-wrapper {
    z-index: 2;

    right: 0;

    width: 280px;
    padding: 50px 36px;

    background-color: ${colors.primary};
    transition: transform 0.3s ease-in-out;
    box-shadow: 0px 0 14px rgba(0, 0, 0, 0.15);

    transform: translateX(${({ isOpen }) => (isOpen ? 0 : '100%')});

    button {
      position: absolute;
      right: 16px;
      top: 16px;
    }
  }
`;
