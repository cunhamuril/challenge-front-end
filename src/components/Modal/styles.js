import styled, { keyframes } from 'styled-components';
import media from 'styled-media-query';
import { fadeIn } from 'react-animations';

import { colors } from '../../theme';

const fadeInAnimation = keyframes`${fadeIn}`;

export const Container = styled.div`
  position: fixed;
  z-index: 1;
  left: 0;
  top: 0;

  width: 100vw;
  height: 100vh;

  display: flex;
  align-items: center;
  justify-content: center;

  ${media.lessThan('medium')`
    padding: 0 32px;
  `}

  ${media.lessThan('small')`
    padding: 0 8px;
  `}
`;

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  background-color: rgba(0, 0, 0, 0.5);
`;

export const Content = styled.div`
  position: relative;
  z-index: 2;

  width: 100%;
  height: 100%;
  max-width: 600px;
  max-height: 500px;
  border-radius: 5px;

  background-color: ${colors.white};
  animation: 0.3s ${fadeInAnimation};

  header .close {
    position: absolute;
    right: 8px;
    top: 8px;

    border: none;
    font-size: 22px;

    cursor: pointer;
    background-color: transparent;
  }

  footer {
    position: absolute;
    bottom: 0;

    display: flex;
    align-items: center;
    justify-content: center;

    width: 100%;
    padding: 16px;
  }
`;
