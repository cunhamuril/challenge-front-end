import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { FiX } from 'react-icons/fi';

import { preventBodyScroll } from '../../functions';

import { Button } from '../.';

import { Overlay, Container, Content } from './styles';

const Modal = ({ children, visible, toggle, ...props }) => {
  useEffect(() => {
    preventBodyScroll(visible);
  }, [visible]);

  return (
    visible && (
      <Container {...props}>
        <Overlay onClick={toggle} />
        <Content>
          <header>
            <button className="close" onClick={toggle}>
              <FiX />
            </button>
          </header>

          <main>{children}</main>

          <footer>
            <Button onClick={toggle}>OK</Button>
          </footer>
        </Content>
      </Container>
    )
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  visible: PropTypes.bool,
  toggle: PropTypes.func,
};

Modal.defaultProps = {
  visible: false,
};

export default Modal;
