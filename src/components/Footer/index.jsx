import React, { useState } from 'react';
import { FiCheckCircle } from 'react-icons/fi';

import { ReactComponent as FacebookLogo } from '../../assets/icons/facebook.svg';
import { ReactComponent as TwitterLogo } from '../../assets/icons/twitter.svg';
import { ReactComponent as GooglePlusLogo } from '../../assets/icons/google-plus.svg';
import { ReactComponent as PinterestLogo } from '../../assets/icons/pinterest.svg';

import { Button } from '../.';

import { Container, Content, FormItem, Submitted } from './styles';

const Footer = () => {
  const [email, setEmail] = useState('');
  const [isInvalid, setIsInvalid] = useState(false);
  const [submitted, setSubmitted] = useState(false);

  const handleEmailValidation = (e) => {
    e.preventDefault();

    setIsInvalid(true);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!email || !email.match(/@/g)) {
      return setIsInvalid(true);
    }

    setIsInvalid(false);

    const payload = {
      siteName: document.title,
      email,
    };

    console.log('Payload: ', payload);

    setEmail('');
    setSubmitted(true);
  };

  return (
    <Container>
      <Content>
        <h1>Keep in touch with us</h1>

        <h5>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
          vitae eros eget tellus tristique bibendum. Donec rutrum sed sem quis
          venenatis.
        </h5>

        {submitted ? (
          <Submitted>
            <FiCheckCircle /> <h4>Your email was successfully sent!</h4>
          </Submitted>
        ) : (
          <form onSubmit={handleSubmit}>
            <FormItem invalid={isInvalid}>
              <input
                type="email"
                placeholder="enter your email to update"
                required
                onInvalid={handleEmailValidation}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormItem>
            <Button type="submit">SUBMIT</Button>
          </form>
        )}

        <div className="social-medias">
          <FacebookLogo />
          <TwitterLogo />
          <GooglePlusLogo />
          <PinterestLogo />
        </div>

        <ul id="address">
          <li>
            Alameda Santos, 1970 <br />
            6th floor - Jardim Paulista <br />
            São Paulo - SP <br />
            +55 11 3090 8500
          </li>

          <li>
            London - UK <br />
            125 Kingsway <br />
            London WC2B 6NH
          </li>

          <li>
            Lisbon - Portugal <br />
            Rua Rodrigues Faria, 103 <br />
            4th floor <br />
            Lisbon - Portugal
          </li>

          <li>
            Curitiba – PR <br />
            R. Francisco Rocha, 198 <br />
            Batel – Curitiba – PR
          </li>

          <li>
            Buenos Aires – Argentina <br />
            Esmeralda 950 <br />
            Buenos Aires B C1007
          </li>
        </ul>
      </Content>
    </Container>
  );
};

export default Footer;
