import styled, { css, keyframes } from 'styled-components';
import media from 'styled-media-query';
import { fadeInDown, zoomIn } from 'react-animations';

import { colors } from '../../theme';

const fadeInDownAnimation = keyframes`${fadeInDown}`;
const zoomInAnimation = keyframes`${zoomIn}`;

export const Container = styled.footer`
  padding: 80px 0;
  min-height: 584px;

  background-color: ${colors.secondary};
`;

export const Content = styled.div.attrs({
  id: 'newsletter',
})`
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;

  width: 100%;
  margin: 0 auto;
  max-width: 1027px;

  text-align: center;

  h1 {
    font-size: 25px;
    line-height: 34px;

    color: ${colors.primary};
    font-family: 'OpenSans-Bold', sans-serif;
  }

  h5 {
    margin-top: 28px;

    width: 100%;
    max-width: 791px;

    font-size: 13px;
    line-height: 18px;

    color: ${colors.lightGray};
    font-family: 'OpenSans-Light', sans-serif;
  }

  form {
    display: flex;
    align-items: stretch;

    width: 100%;
    max-width: 578px;
    margin-top: 50px;

    button {
      margin-left: 20px;      
    }
  }

  .social-medias {
    margin-top: 53px;

    svg {
      cursor: pointer;
      transition: filter 0.3s ease;

      :hover {
        filter: brightness(1.05);
      }

      :active {
        filter: brightness(1.1);
      }

      + svg {
        margin-left: 51.13px;
      }
    }
  }

  #address {
    display: grid;
    grid-gap: 58px;  
    grid-template-columns: repeat(5, 1fr);

    width: 100%;
    margin-top: 63.25px;

    list-style: none;

    li {
      font-size: 13px;
      text-align: start;
      line-height: 1.7;
      color: ${colors.white};
    }
  }

  ${media.lessThan('large')`
    padding: 0 64px;
    
    #address {
      grid-gap: 32px;
    }
  `}

  ${media.lessThan('medium')`
    #address {
      grid-template-columns: repeat(3, 1fr);
      grid-template-rows: repeat(2, 1fr);
    }
  `}

  ${media.lessThan('small')`
    padding: 0 16px;

    form button {
      margin-left: 4px;
    }

    .social-medias svg + svg {
      margin-left: 16px;
    }

    #address {
      grid-template-columns: repeat(2, 1fr);
      grid-template-rows: repeat(3, 1fr);
    }
  `}
`;

export const FormItem = styled.div`
  position: relative;

  width: 100%;
  border-radius: 5px;

  transition: border 0.3s ease;
  border: solid 1px transparent;

  input {
    height: 100%;
    width: inherit;
    padding: 5px 15px;

    border: none;
    border-radius: inherit;

    font-size: 13px;
    line-height: 18px;

    :invalid {
    }
  }

  ${({ invalid }) =>
    invalid &&
    css`
      border: solid 1px ${colors.red};

      &::after {
        content: 'A valid e-mail is required';

        position: absolute;
        bottom: -20px;
        right: 0;

        font-size: 12px;
        color: ${colors.red};
        animation: 0.3s ${fadeInDownAnimation};
      }
    `}
`;

export const Submitted = styled.div`
  display: flex;
  align-items: center;

  margin-top: 50px;

  animation: 0.5s ${zoomInAnimation};

  svg {
    font-size: 30px;
    color: ${colors.green};
  }

  h4 {
    margin-left: 8px;
    color: ${colors.white};
  }
`;
