import styled from 'styled-components';

import { colors } from '../../theme';

export const Container = styled.button`
  padding: 6px 30px;

  border: none;
  cursor: pointer;
  border-radius: 5px;
  transition: filter 0.3s ease;

  color: ${colors.lightBlack};
  background-color: ${colors.primary};
  font-family: 'OpenSans-Bold', sans-serif;

  :hover {
    filter: brightness(1.05);
  }

  :active {
    filter: brightness(1.1);
  }
`;
