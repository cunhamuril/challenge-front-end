import styled from 'styled-components';

import { colors } from '../../theme';

export const Container = styled.button`
  position: fixed;
  bottom: 32px;
  right: 32px;

  display: flex;
  align-items: center;
  justify-content: center;

  width: 40px;
  height: 40px;

  border: none;
  font-size: 22px;

  cursor: pointer;
  border-radius: 5px;
  transition: filter 0.3s ease;
  background-color: ${colors.primary};
  box-shadow: 0 0 16px rgba(0, 0, 0, 0.15);

  :hover {
    filter: brightness(1.05);
  }

  :active {
    filter: brightness(1.1);
  }
`;
