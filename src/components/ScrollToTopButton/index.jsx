import React from 'react';

import { FiChevronUp } from 'react-icons/fi';

import { Container } from './styles';

const ScrollToTopButton = () => {
  const scrollToTop = () => {
    const action =
      document.documentElement.scrollTop || document.body.scrollTop;
    if (action > 0) {
      window.requestAnimationFrame(scrollToTop);
      window.scrollTo(0, action - action / 8);
    }
  };

  return (
    <Container onClick={scrollToTop}>
      <FiChevronUp />
    </Container>
  );
};

export default ScrollToTopButton;
