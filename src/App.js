import React from 'react';

import { Header, Footer } from './components';
import { Main } from './pages';

import { GlobalStyle } from './theme';

function App() {
  return (
    <>
      <GlobalStyle />

      <Header />
      <Main />
      <Footer />
    </>
  );
}

export default App;
